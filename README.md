:warning: Work in progress! (not really tested, no limit on inotify watches, etc)

# luv-vcprompt
A vcprompt written in lua using luv. To be fast it spawns a daemon to monitor visited folders with inotify.
Only git is supported.

## Installation
For Linux/bash.
- Install lua and luv
- Clone this repo anywhere then use in your prompt, for example in bashrc :

~~~~
export PS1='\u@\h \w $(lua /path/to/luv_vcprompt.lua)> '
~~~~
