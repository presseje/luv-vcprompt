local uv = require("luv")
local basepath = arg[0]:match("(.*)/")
package.path=package.path..";"..basepath.."/?.lua"
local utils = require("utils")

local git_path, connected, not_launched
local server_pipe = uv.new_pipe()

local global_timer = uv.new_timer()
local function start_timeout()
    global_timer:start(10, 0, function()
        print (" (...) ")
        uv.stop()
    end)
end

local function send_path(pipe)
    assert (pipe:write(git_path))
    pipe:read_start(function(err, data)
        assert (not err, err)
        if data then data = " "..data end
        print (data)
        pipe:read_stop()
        uv.stop()
    end)
end

local function launch_daemon()
    uv.spawn (
        "lua",
        {
            args = {basepath.."/luv_vcprompt_daemon.lua", git_path, basepath},
            detached = true,
        }
    )

    local timer_wait = uv.new_timer()
    global_timer:stop()
    timer_wait:start (20, 0, function()
        local retry_pipe = uv.new_pipe() --TODO fails if try to reuse server_pipe?
        retry_pipe:connect("/tmp/luv-vcprompt/socket", function (err)
            if err then
                print (" (launching...) ")
                uv.stop()
            else
                connected = true
                start_timeout()
                if git_path then send_path(retry_pipe) end
            end
        end)
    end)
end

local function find_git_path()
    utils.exec (nil, "git", {"rev-parse", "--show-toplevel"}, function (out)
        if out and #out > 0 then
            git_path = out:gsub('\n','')
            if connected then send_path(server_pipe) end
        else
            uv.stop()
        end
        if not_launched then
            launch_daemon()
        end
    end)
end

local function connect_daemon()
    server_pipe:connect("/tmp/luv-vcprompt/socket", function (err)
        if err then
            if err == "ENOENT" or err == "ECONNREFUSED" then
                not_launched = true
                if git_path then launch_daemon() end
            else
                error(err)
            end
        else
            connected = true
            if git_path then send_path(server_pipe) end
        end
    end) --TODO socket path from env? use $HOME?
end

start_timeout()
find_git_path()
connect_daemon()

uv.run()
