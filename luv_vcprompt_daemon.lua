local uv = require("luv")
local basepath = arg[0]:match("(.*)/")
package.path=package.path..";"..basepath.."/?.lua"
local utils = require("utils")

local status_tab = {}

local function add_watch (path, root_path, events, callback)
    if not events[path] then
        print ("installing watch on subfolder "..path.." of repo "..root_path)
        local event = uv.new_fs_event()
        --TODO store and check number of watches
        assert(event:start(path, {}, function(err, filename, events) --recursive=true not supported with inotify
            assert (not err, err)
            -- TODO maybe can ignore renamed?
            print ("watch trigd on "..path)
            callback()
            -- TODO if it's a new folder, watch it if not already
            -- TODO check that path is still a folder and still in root_path or remove watch
        end))
        events[path] = event;
    end

    -- (re)update status of root path after any watch is installed
    callback()
end

-- Add watches on this folder + all subdirs
local function add_watch_subdir(path, root_path, events, callback) --TODO move to utils
    -- recurse in all subfolders
    local dir = assert(uv.fs_opendir(path, nil, 16)) --TODO can be async
    local function next_dir()
        uv.fs_readdir (dir, function (err, entries)
            assert (not err, err)
            if entries and #entries then
                for _,entry in pairs(entries) do
                    if entry.type == "directory" then
                        --print ("recursing to subfolder "..entry.name.." of "..path)
                        add_watch_subdir(path.."/"..entry.name, root_path, events, callback)
                    end
                end
                next_dir()
            else
                dir:closedir() --Warning will Crash if done after watch is installed!!
            end
        end)
    end
    next_dir()

    -- Install watch on current folder
    add_watch (path, root_path, events, callback)
end

-- Install watches on all subfolders containing git-tracked files
local function add_watch_dirlist (path, root_path, events, callback)
    -- Match all subdirs from the list of repo files
    utils.exec (path, "git", {"ls-files", "-z"}, function (list)
        local first = true
        for dir in list:gmatch ("([^\0]+)/[^\0/]-\0") do
            add_watch (path.."/"..dir, root_path, events, callback)
        end
    end)
    --Files at root won't match the pattern
    add_watch (path, root_path, events, callback)
end

-- Install watches on a git repository
local function add_watches_root (root_path, callback)
    local events = {}
    add_watch_subdir(root_path.."/.git", root_path, events, callback)
    add_watch_dirlist(root_path, root_path, events, callback)
    return events
end

status_tab.add_watches = function(self, path)
    local events = {}
    local callback = function() self:update(path) end
    self[path] = {
        state = "init",
    }
    self[path].events = add_watches_root (path, callback)
    print ("added watches on "..path)
end

status_tab.rm_watches = function (self, path)
    if self[path] and self[path].event then
        print ("stopping watch on "..path)
        for event in self[path].events do event:stop() end
        self[path] = nil
    end
end

status_tab.update = function (self, path)
    if self[path].state == "updating" then
        self.need_reupdate = true
        return
    end
    self[path].state = "updating"
    utils.exec (path, "git", {"--no-optional-locks", "status", "-zb"}, function(out)
        local branch_info, files_info = out:match ("## (.-)\0(.*)")
        local u, m, a, d

        for status, file in files_info:gmatch (" *(.-) (.-)\0") do
            if status == "??" then u = true end
            if status:match("M") then m = true end
            if status:match("D") then d = true end
            if status:match("A") then a = true end
            -- TODO all types from doc
        end

        utils.exec (path, "git", {"describe", "--tags", "--exact-match"}, function(out)
            if self[path] then
                local t
                if out and #out then
                    t = out:match ('(%g+)')
                end

                self[path].status = string.format("[%s%s%s%s%s%s%s]",
                    branch_info, t and ' ('..t..')' or '', (m or a or u or d) and ' ' or '' ,
                    m and 'M' or '', a and '+' or '', u and '?' or '', d and '-' or '')
                self[path].state = "ready"
                if self.need_reupdate then
                    self.need_reupdate = false
                    self:update(path)
                end
            end
        end)
    end)
end

if arg[1] then status_tab:add_watches(arg[1]) end

local function open_socket ()
    local res, err, errnum = uv.fs_mkdir("/tmp/luv-vcprompt", 0x1FF)
    if not res then
        if errnum == "EEXIST" then
            uv.fs_unlink("/tmp/luv-vcprompt/socket")
        else
            error(err)
        end
    end
    local pipe = uv.new_pipe()
    assert(pipe:bind("/tmp/luv-vcprompt/socket"))
    assert(pipe:listen(16, function()
        local client = uv.new_pipe()
        pipe:accept(client)
        client:read_start(function(err, path)
            assert (not err, err)
            if path then
                if not status_tab[path] then
                    status_tab:add_watches(path)
                end
                if status_tab[path].status then client:write(status_tab[path].status) end
                if status_tab[path].state ~= "ready" then client:write("("..status_tab[path].state..")") end
                client:close()
            end
        end)
    end))
end

open_socket()

-- Avoid sigpipe when server answers to already timedout client
local signal = uv.new_signal()
uv.signal_start(signal, "sigpipe")

uv.run()
