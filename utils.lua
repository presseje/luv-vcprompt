local uv = require ("luv")

local already_spawned = 0

local function exec (path, cmd, args, callback, no_concat)
    if already_spawned >= 256 then
        local delay = assert(uv.new_timer())
        delay:start(10, 0, function ()
            exec (path, cmd, args, callback)
        end)
    else
        local stdout = uv.new_pipe()
        local data = ""
        already_spawned = already_spawned + 1
        assert(uv.spawn (cmd,
        {
            stdio = {nil, stdout, nil},
            args = args,
            cwd = path,
        }))
        assert(stdout:read_start (function(err, chunk)
            assert(not err, err)
            if chunk then
                if no_concat then
                    callback(chunk)
                else
                    data = data..chunk
                end
            else
                stdout:close()
                already_spawned = already_spawned - 1
                if not no_concat then
                    callback(data)
                end
            end
        end))
    end
end

return {
    exec = exec,
}